import React, {Component} from 'react';
import logo from '../assets/logo.png';
import {View, StyleSheet, Image, Text} from 'react-native';

export default class Logo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image style={styles.image} source={logo} />
          <Text style={styles.logoText}>Movie Review</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'center',
    margin: 40,
  },
  logo: {
    justifyContent: 'center',
    flexDirection: 'column',
  },

  image: {
    width: 137,
    height: 121,
    alignSelf: 'center',
  },

  logoText: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    color: '#F6F7F7',
  },
});
