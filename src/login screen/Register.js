import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Logo from './Logo';
import {
  TextInput,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import {Button} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import {API_REGISTER} from '../API';
import {PROCCESS, SUCCESS, FAILED, ISLOGIN} from '../Redux/Reducer/case';
import {useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

const Register = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [Cpassword, setCPassword] = useState('');
  const dispatch = useDispatch();

  const onChangeCPassword = val => {
    setCPassword(val);
  };

  const onChangeEmail = val => {
    setEmail(val);
  };

  const onChangePassword = val => {
    setPassword(val);
  };

  const onChangeUsername = val => {
    setUsername(val);
  };

  const handleRegister = async () => {
    dispatch({type: PROCCESS});
    if (password == Cpassword) {
      try {
        const res = await Axios.post(API_REGISTER, {
          name: username,
          email: email,
          password: password,
        });

        if (res !== null) {
          const status = res.data.status;
          const data = res.data;
          console.log(status);
          if (status == 'success') {
            dispatch({type: ISLOGIN});
            dispatch({type: SUCCESS});
            await AsyncStorage.setItem('userToken', data.data.token);
            navigation.navigate('Home');
          } else {
            dispatch({type: FAILED});
          }
        }
      } catch (error) {
        console.log(error, 'error');
        dispatch({type: FAILED});
      }
    } else {
      alert('Password not match');
      dispatch({type: FAILED});
    }

    console.log(username, email, password);
  };

  return (
    <View style={styles.container}>
      <Logo />
      <TextInput
        style={styles.inputBox}
        placeholder="Username"
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangeUsername}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Email"
        keyboardType="email-address"
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangeEmail}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Password"
        secureTextEntry={true}
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangePassword}
      />
      <TextInput
        style={styles.inputBox}
        placeholder="Confirm Password"
        secureTextEntry={true}
        underlineColorAndroid="#B4B4B0"
        placeholderTextColor="#B4B4B0"
        selectionColor="#B4B4B0"
        onChangeText={onChangeCPassword}
      />
      <Button
        title="Register"
        buttonStyle={styles.button}
        titleStyle={styles.signupButton}
        onPress={handleRegister}
      />
      <View style={styles.signinTextCont}>
        <Text style={styles.signinText}>Already have account?</Text>
        <TouchableWithoutFeedback onPress={() => navigation.navigate('Login')}>
          <Text style={styles.signinButton}>Sign In</Text>
        </TouchableWithoutFeedback>
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#040303',
    alignContent: 'center',
  },
  inputBox: {
    fontFamily: 'Roboto',
    fontSize: 18,
    lineHeight: 21,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'left',
    color: '#F6F7F7',
    margin: 10,
  },
  button: {
    margin: 10,
    alignSelf: 'center',
    borderRadius: 100,
    width: 124,
    backgroundColor: '#FEA800',
  },
  signupButton: {
    color: '#040303',
  },
  signinText: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: 15,
    lineHeight: 18,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'right',
    color: '#F6F7F7',
    marginHorizontal: 5,
  },
  signinButton: {
    fontWeight: 'bold',
    color: '#F6F7F7',
  },
  signinTextCont: {
    justifyContent: 'center',
    paddingVertical: 16,
    flexDirection: 'row',
    margin: 10,
  },
});
