import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Review from './Review';
import MyReview from './MyReview';

const Stack = createStackNavigator();
const ReviewRoutes = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="All Review" component={Review} />
      <Stack.Screen name="My Review" component={MyReview} />
    </Stack.Navigator>
  );
};

export default ReviewRoutes;
