import React, {useState, useEffect} from 'react';
import {Card, Image, Icon} from 'react-native-elements';
import {View, StyleSheet, Text} from 'react-native';
import Avatar from '../assets/avatar.png';
import {useDispatch, useSelector} from 'react-redux';
import {PROCCESS, SUCCESS, FAILED} from '../Redux/Reducer/case';
import Axios from 'axios';
import {API_MOVIE} from '../API';
import {FlatList} from 'react-native-gesture-handler';

const CardReview = () => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const id = useSelector(state => state.auth.id);
  useEffect(() => {
    getData();
  });
  const getData = async () => {
    try {
      const res = await Axios.get(`${API_MOVIE}${id}`);

      if (res !== null) {
        const data = res.data.data.detailReviews;
        setData(data);

        dispatch({type: SUCCESS});
      } else {
        dispatch({type: SUCCESS});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: FAILED});
    }
  };

  const renderItem = ({item, index}) => (
    <Card containerStyle={styles.container}>
      <View style={styles.titleReview}>
        <Image
          source={{uri: item.detailProfile.avatar}}
          defaultSource={Avatar}
          style={styles.titleImage}
        />
        <View style={styles.ratting}>
          <View style={styles.titleContainer}>
            <Icon
              type="material-community"
              name="star"
              size={20}
              color="rgba(255, 194, 0, 0.98)"
              containerStyle={styles.ratingIcon}
            />
            <Text style={styles.ratingText}>{item.rating}/10</Text>
            <Text style={styles.title}>{item.title}</Text>
          </View>
          <View style={styles.creditContainer}>
            <Text>Reviewer: </Text>
            <Text style={styles.userCredit}>{item.detailProfile.name}</Text>
          </View>
        </View>
      </View>
      <View style={styles.commentContainer}>
        <Text style={styles.container}>{item.description}</Text>
      </View>
    </Card>
  );

  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={item => item.id}
    />
  );
};

export default CardReview;

const styles = StyleSheet.create({
  titleReview: {
    flexDirection: 'row',
  },
  creditContainer: {
    flexDirection: 'row',
    marginHorizontal: 5,
  },
  titleContainer: {
    flexDirection: 'row',
  },
  ratting: {
    flexDirection: 'column',
  },
  titleImage: {
    width: 40,
    height: 40,
  },
  title: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'justify',
    textAlignVertical: 'top',
    letterSpacing: -0.2,
    color: '#000',
    marginHorizontal: 10,
    top: -3,
  },
  ratingText: {
    textAlign: 'justify',
  },
  userCredit: {
    fontFamily: 'Roboto',
    fontSize: 14.5,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#000',
    fontWeight: 'bold',
  },
  commentContainer: {
    marginVertical: 10,
  },
  comment: {
    fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#000',
  },
  container: {
    width: 325,
    height: 104,
    borderRadius: 20,
  },
});
