export const API_REGISTER =
  'https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/register';
export const API_LOGIN =
  'https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/login';
export const API_USER =
  'https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user';
export const API_PASSWORD =
  'https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/user/updatepassword';
export const API_IMAGE = 'https://image.tmdb.org/t/p/w500';
export const API_MOVIE =
  'https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/';
