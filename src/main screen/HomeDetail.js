import React from 'react';
import {StyleSheet, View} from 'react-native';
import {SearchBar, Icon} from 'react-native-elements';
import CardItemDetail from './CardItemDetail';
import AwesomeAlert from 'react-native-awesome-alerts';
import Alert from '../Alert';

const HomeDetail = () => {
  return (
    <View style={styles.container}>
      <CardItemDetail />
      <Alert />
    </View>
  );
};

export default HomeDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    backgroundColor: '#040303',
  },
  searchContainer: {
    backgroundColor: '#F6F7F7',
    borderRadius: 10,
  },
  searchBarContainer: {
    backgroundColor: '#040303',
    margin: 10,
  },
});
