import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Share} from 'react-native';
import {Card, Button, Icon, Image} from 'react-native-elements';
import {
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native-gesture-handler';
import {useSelector, useDispatch} from 'react-redux';
import {PROCCESS, SUCCESS, COMMENT, DETAIL, ALERT} from '../Redux/Reducer/case';
import Axios from 'axios';
import {API_MOVIE, API_IMAGE} from '../API';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';
import AwesomeAlert from 'react-native-awesome-alerts';
import Alert from '../Alert';

const CardItemDetail = () => {
  const [data, setData] = useState([]);
  const [alert, setAlert] = useState(false);
  const [genre, setGenre] = useState('');
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const id = useSelector(state => state.auth.id);
  const isLogin = useSelector(state => state.auth.isLogin);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    dispatch({type: PROCCESS});
    try {
      const res = await Axios.get(`${API_MOVIE}${id}`);

      if (res !== null) {
        const dataMovie = res.data.data.movie;
        setData(dataMovie);
        setGenre(dataMovie.genres[0].name);
        dispatch({type: SUCCESS});
        console.log(genre);
      } else {
        console.log('error');
        dispatch({type: SUCCESS});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: SUCCESS});
    }
  };

  const handleShare = async url => {
    try {
      const result = await Share.share(
        {
          message: url,
          title: 'hey check this movie',
        },
        {
          dialogTitle: 'Share to...',
        },
      );
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('Shared Activity');
        } else {
          // shared
          console.log('Link Copied');
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
        console.log('Share canceled');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const comment = id => {
    if (isLogin == true) {
      dispatch({type: DETAIL, payload: id});
      dispatch({type: COMMENT});
    } else {
      // alert('Please Login or Register for Review');
      dispatch({type: ALERT});
    }
  };

  const allComment = id => {
    dispatch({type: DETAIL, payload: id});
    navigation.navigate('All Review');
  };

  return (
    <ScrollView>
      <View>
        <Card containerStyle={styles.cardContainer}>
          <View style={styles.imageContainer}>
            <TouchableWithoutFeedback>
              <Image
                source={{uri: `${API_IMAGE}${data.backdrop_path}`}}
                style={styles.image}
              />
            </TouchableWithoutFeedback>
            <Button
              icon={
                <Icon
                  type="material-community"
                  name="play-circle-outline"
                  size={40}
                  color="#FEA800"
                />
              }
              type="clear"
              containerStyle={styles.imageButton}
            />
          </View>
          <View style={styles.titleDetailContainer}>
            <Text style={styles.titleDetail}>{data.title}</Text>
            <Text style={styles.titleGenre}>
              {moment(data.release_date).format('YYYY')}| {genre}
            </Text>
          </View>
          <View style={styles.commentContainer}>
            <Image
              source={{uri: `${API_IMAGE}${data.poster_path}`}}
              style={styles.commentImage}
            />
            <View>
              <View style={styles.ratingContainer}>
                <View style={styles.reviewContainer}>
                  <Icon
                    type="material-community"
                    name="star"
                    size={20}
                    color="rgba(255, 194, 0, 0.98)"
                    containerStyle={styles.ratingIcon}
                  />
                  <Text style={styles.ratingText}>{data.vote_average}/10</Text>
                </View>
                <Button
                  title="Rate This"
                  icon={
                    <Icon
                      type="material-community"
                      name="star"
                      size={20}
                      color="#979797"
                    />
                  }
                  onPress={() => comment(data.id)}
                  type="clear"
                  buttonStyle={styles.buttonRating}
                  containerStyle={styles.buttonRatingContainer}
                  titleStyle={styles.buttonTitle}
                />
              </View>
              <View style={styles.commentTextContainer}>
                <Text style={styles.commentText}>{data.overview}</Text>
              </View>
            </View>
          </View>
          <View style={styles.containerButton}>
            <Button
              icon={
                <Icon
                  name="comment-text-outline"
                  type="material-community"
                  color="#040303"
                  style={{marginRight: 10}}
                />
              }
              title={data.vote_count}
              type="clear"
              titleStyle={styles.titleButton}
              onPress={() => allComment(data.id)}
            />
            <Button
              icon={
                <Icon
                  name="share-variant"
                  type="material-community"
                  color="#040303"
                />
              }
              type="clear"
              onPress={() => handleShare(data.homepage)}
            />
          </View>
        </Card>
      </View>
    </ScrollView>
  );
};

export default CardItemDetail;

const styles = StyleSheet.create({
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    paddingTop: 3,
  },
  cardContainer: {
    borderRadius: 20,
  },
  commentText: {
    marginVertical: 15,
    fontFamily: 'Roboto',
    fontSize: 14,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#040303',
  },
  cardButton: {
    marginHorizontal: 10,
  },
  titleButton: {
    fontFamily: 'Roboto',
    fontSize: 14,
    letterSpacing: -0.2,
    color: '#040303',
  },
  imageContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image: {
    width: 300,
    height: 170,
  },
  imageButton: {
    position: 'absolute',
    alignSelf: 'center',
  },
  titleDetailContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    marginTop: 15,
    borderBottomWidth: 1,
  },
  titleDetail: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 24,
    letterSpacing: -0.2,
    color: '#040303',
    maxWidth: 210,
  },
  titleGenre: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 18,
    letterSpacing: -0.2,
    color: '#040303',
    alignSelf: 'center',
    maxWidth: 150,
  },
  commentImage: {
    width: 90,
    height: 132,
  },
  commentContainer: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  ratingContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
  buttonRating: {
    flexDirection: 'column',
  },
  buttonRatingContainer: {
    alignSelf: 'center',
  },
  reviewContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    marginRight: 15,
  },
  ratingIcon: {
    alignSelf: 'center',
  },
  ratingText: {
    alignSelf: 'center',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#040303',
    letterSpacing: -0.2,
  },
  buttonTitle: {
    fontFamily: 'Roboto',
    fontSize: 14,
    letterSpacing: -0.2,
    color: '#040303',
  },
  commentTextContainer: {
    marginHorizontal: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    width: 240,
  },
});
