import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {SearchBar, Icon, Button, Card, Image} from 'react-native-elements';
import {
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import Axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {PROCCESS, SUCCESS, DETAIL, COMMENT} from '../Redux/Reducer/case';
import {API_MOVIE, API_IMAGE} from '../API';
import {useNavigation} from '@react-navigation/native';

const HomeGenre = () => {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const genreID = useSelector(state => state.auth.genreID);
  const genre = useSelector(state => state.auth.genreName);
  const navigation = useNavigation();
  useEffect(() => {
    getData();
  }, [getData]);

  const getData = async () => {
    dispatch({type: PROCCESS});
    try {
      const res = await Axios.get(`${API_MOVIE}genre/${genreID}`);

      if (res !== null) {
        const dataMovie = res.data.data.results;
        setData(dataMovie);
        dispatch({type: SUCCESS});
      } else {
        console.log('error');
        dispatch({type: SUCCESS});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: SUCCESS});
    }
  };

  const detail = id => {
    dispatch({type: DETAIL, payload: id});
    navigation.navigate('Home Details');
  };

  const comment = id => {
    dispatch({type: DETAIL, payload: id});
    navigation.navigate('All Review');
  };

  const renderItem = ({item, index}) => (
    <Card containerStyle={styles.cardContainer}>
      <View style={styles.imageContainer}>
        <TouchableWithoutFeedback onPress={() => detail(item.id)}>
          <Image
            source={{uri: `${API_IMAGE}${item.backdrop_path}`}}
            style={styles.image}
          />
        </TouchableWithoutFeedback>
        <Button
          icon={
            <Icon
              type="material-community"
              name="play-circle-outline"
              size={40}
              color="#FEA800"
            />
          }
          type="clear"
          containerStyle={styles.imageButton}
          onPress={() => detail(item.id)}
        />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
      <Text style={styles.commentText}>{item.overview}</Text>
      <View style={styles.containerButton}>
        <Button
          icon={
            <Icon
              name="comment-text-outline"
              type="material-community"
              color="#040303"
              style={{marginRight: 10}}
            />
          }
          title={item.vote_count}
          type="clear"
          titleStyle={styles.titleButton}
          onPress={() => comment(item.id)}
        />
        <Button
          icon={
            <Icon name="share-variant" type="material-community" color="#040303" onPress={() => detail(item.id)}/>
          }
          type="clear"
        />
      </View>
    </Card>
  );
  return (
    <View style={styles.container}>
      {/* <SearchBar
        placeholder="Type here ...."
        searchIcon={
          <Icon
            type="material-community"
            name="magnify"
            size={20}
            color="#979797"
          />
        }
        value={search}
        onChangeText={onChangeSearch}
        inputContainerStyle={styles.searchContainer}
        containerStyle={styles.searchBarContainer}
      /> */}
      {/* <View style={styles.titleContainer}>
        <Text style={styles.titleGenre}>Best Genre</Text>
        <Text style={styles.genreDetail}>more >></Text>
      </View> */}
      <View style={styles.genreListContainer}>
        {/* <FlatList
          horizontal
          data={genres}
          renderItem={renderButton}
          keyExtractor={item => item.id}
        /> */}
      </View>
      <View>
        <Text style={styles.genreListTitle}>Hot {genre} Movie</Text>
      </View>
      <ScrollView>
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={item => item.id}
        />
      </ScrollView>
    </View>
  );
};

export default HomeGenre;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderBottomEndRadius: 20,
    borderBottomStartRadius: 20,
    backgroundColor: '#040303',
  },
  searchContainer: {
    backgroundColor: '#F6F7F7',
    borderRadius: 10,
  },
  searchBarContainer: {
    backgroundColor: '#040303',
    margin: 10,
  },
  titleGenre: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    letterSpacing: -0.2,
    color: '#F6F7F7',
  },
  genreDetail: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 12,
    letterSpacing: -0.2,
    color: '#F6F7F7',
    alignSelf: 'flex-end',
  },
  genreListContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 10,
  },
  genreListButton: {
    backgroundColor: '#F6F7F7',
    marginHorizontal: 5,
    borderRadius: 6,
  },
  genreListButtonTitle: {
    color: '#040303',
  },
  genreListButtonContainer: {
    marginRight: 5,
  },
  genreListTitle: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    letterSpacing: -0.2,
    marginHorizontal: 20,
    marginVertical: 5,
    color: '#F6F7F7',
    textAlign: 'justify',
    alignSelf: 'center',
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    paddingTop: 3,
  },
  cardContainer: {
    borderRadius: 20,
  },
  commentText: {
    marginVertical: 15,
    fontFamily: 'Roboto',
    fontSize: 14,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#040303',
  },
  cardButton: {
    marginHorizontal: 10,
  },
  titleButton: {
    fontFamily: 'Roboto',
    fontSize: 14,
    letterSpacing: -0.2,
    color: '#040303',
  },
  imageContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image: {
    width: 300,
    height: 170,
  },
  imageButton: {
    position: 'absolute',
    alignSelf: 'center',
  },
  titleContainer: {
    justifyContent: 'center',
    marginTop: 5,
  },
  title: {
    fontFamily: 'Roboto',
    fontSize: 20,
    letterSpacing: -0.2,
    color: '#040303',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
