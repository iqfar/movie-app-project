import React, {useState, useEffect} from 'react';
import {FlatList} from 'react-native-gesture-handler';
import Axios from 'axios';
import {View, Text} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {PROCCESS, SUCCESS, DETAIL, COMMENT} from '../Redux/Reducer/case';
import {StyleSheet} from 'react-native';
import {Card, Button, Icon, Image} from 'react-native-elements';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {API_MOVIE, API_IMAGE} from '../API';

const CardItemListGenre = () => {
  const [data, setData] = useState([]);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const genreID = useSelector(state => state.auth.genreID);

  useEffect(() => {
    getData();
  }, [getData]);

  const getData = async () => {
    dispatch({type: PROCCESS});
    try {
      const res = await Axios.get(`${API_MOVIE}/genre/${genreID}`);
      console.log(genreID);
      if (res !== null) {
        const dataMovie = res.data.data.results;
        setData(dataMovie);
        dispatch({type: SUCCESS});
      } else {
        console.log('error');
        dispatch({type: SUCCESS});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: SUCCESS});
    }
  };

  const detail = id => {
    dispatch({type: DETAIL, payload: id});
    navigation.navigate('Home Details');
  };

  const comment = id => {
    dispatch({type: DETAIL, payload: id});
    navigation.navigate('All Review');
  };

  const renderItem = ({item, index}) => (
    <Card containerStyle={styles.cardContainer}>
      <View style={styles.imageContainer}>
        <TouchableWithoutFeedback onPress={() => detail(item.id)}>
          <Image
            source={{uri: `${API_IMAGE}${item.backdrop_path}`}}
            style={styles.image}
          />
        </TouchableWithoutFeedback>
        <Button
          icon={
            <Icon
              type="material-community"
              name="play-circle-outline"
              size={40}
              color="#FEA800"
            />
          }
          type="clear"
          containerStyle={styles.imageButton}
          onPress={() => detail(item.id)}
        />
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{item.title}</Text>
      </View>
      <Text style={styles.commentText}>{item.overview}</Text>
      <View style={styles.containerButton}>
        <Button
          icon={
            <Icon
              name="comment-text-outline"
              type="material-community"
              color="#040303"
              style={{marginRight: 10}}
            />
          }
          title={item.vote_count}
          type="clear"
          titleStyle={styles.titleButton}
          onPress={() => comment(item.id)}
        />
        <Button
          icon={
            <Icon name="share-variant" type="material-community" color="#040303" />
          }
          type="clear"
          onPress={() => detail(item.id)}
        />
      </View>
    </Card>
  );

  return (
    <FlatList
      data={data}
      renderItem={renderItem}
      keyExtractor={item => item.id}
    />
  );
};

export default CardItemListGenre;

const styles = StyleSheet.create({
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    paddingTop: 3,
  },
  cardContainer: {
    borderRadius: 20,
  },
  commentText: {
    marginVertical: 15,
    fontFamily: 'Roboto',
    fontSize: 14,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#040303',
  },
  cardButton: {
    marginHorizontal: 10,
  },
  titleButton: {
    fontFamily: 'Roboto',
    fontSize: 14,
    letterSpacing: -0.2,
    color: '#040303',
  },
  imageContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image: {
    width: 300,
    height: 170,
  },
  imageButton: {
    position: 'absolute',
    alignSelf: 'center',
  },
  titleContainer: {
    justifyContent: 'center',
    marginTop: 5,
  },
  title: {
    fontFamily: 'Roboto',
    fontSize: 20,
    letterSpacing: -0.2,
    color: '#040303',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
