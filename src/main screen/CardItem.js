import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Card, Button, Icon, Image} from 'react-native-elements';
import trailer from '../assets/Movie.png';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const CardItem = () => {
  const navigation = useNavigation();
  return (
    <Card containerStyle={styles.cardContainer}>
      <View style={styles.imageContainer}>
        <TouchableWithoutFeedback>
          onPress={() => navigation.navigate('Home Details')}>
          <Image source={trailer} style={styles.image} />
        </TouchableWithoutFeedback>
        <Button
          icon={
            <Icon
              type="material-community"
              name="play-circle-outline"
              size={40}
              color="#FEA800"
            />
          }
          type="clear"
          containerStyle={styles.imageButton}
        />
      </View>

      <Text style={styles.commentText}>
        A poor family, the Kims, con their way into becoming the servants of a
        rich family, the Parks. But their easy life gets complicated when their
        deception is threatened with exposure.
      </Text>
      <View style={styles.containerButton}>
        <Button
          icon={
            <Icon
              name="comment-text-outline"
              type="material-community"
              color="#040303"
              style={{marginRight: 10}}
            />
          }
          title="123"
          type="clear"
          titleStyle={styles.titleButton}
        />
        <Button
          icon={
            <Icon name="share-variant" type="material-community" color="#040303" />
          }
          type="clear"
        />
      </View>
    </Card>
  );
};

export default CardItem;

const styles = StyleSheet.create({
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    paddingTop: 3,
  },
  cardContainer: {
    borderRadius: 20,
  },
  commentText: {
    marginVertical: 15,
    fontFamily: 'Roboto',
    fontSize: 14,
    textAlign: 'justify',
    letterSpacing: -0.2,
    color: '#040303',
  },
  cardButton: {
    marginHorizontal: 10,
  },
  titleButton: {
    fontFamily: 'Roboto',
    fontSize: 14,
    letterSpacing: -0.2,
    color: '#040303',
  },
  imageContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  image: {
    width: 300,
    height: 170,
  },
  imageButton: {
    position: 'absolute',
    alignSelf: 'center',
  },
});
