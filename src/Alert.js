import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Modal from 'react-native-modal';

import AwesomeAlert from 'react-native-awesome-alerts';
import {useDispatch, useSelector} from 'react-redux';
import {ALERT} from './Redux/Reducer/case';
import {useNavigation} from '@react-navigation/native';

const Alert = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight = Dimensions.get('window').height;
  const isAlert = useSelector(state => state.auth.alert);
  return (
    <Modal
      isVisible={isAlert}
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}>
      <AwesomeAlert
        show={true}
        showProgress={false}
        title="Oops"
        message="Please Login or Register for Review"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={true}
        showConfirmButton={true}
        cancelText="Cancel"
        confirmText="OK"
        confirmButtonColor="#DD6B55"
        onCancelPressed={() => {
          // this.hideAlert();
          dispatch({type: ALERT});
        }}
        onConfirmPressed={() => {
          // this.hideAlert();
          navigation.navigate('Login');
          dispatch({type: ALERT});
        }}
      />
    </Modal>
  );
};

export default Alert;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  button: {
    margin: 10,
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderRadius: 5,
    backgroundColor: '#AEDEF4',
  },
  text: {
    color: '#fff',
    fontSize: 15,
  },
});
