import React, {useState} from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import Modal from 'react-native-modal';
import StarRating from 'react-native-star-rating';
import {TextInput} from 'react-native-gesture-handler';
import {Button} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {PROCCESS, SUCCESS, FAILED, COMMENT} from './Redux/Reducer/case';
import Axios from 'axios';
import {API_MOVIE} from './API';
import AsyncStorage from '@react-native-community/async-storage';

const ModalRate = () => {
  //   const [isModalVisible, setModalVisible] = useState(false);
  const [starRate, setStarRate] = useState(0);
  const [title, setTitle] = useState('');
  const [review, setReview] = useState('');
  const id = useSelector(state => state.auth.id);
  const modal = useSelector(state => state.auth.isComment);
  const dispatch = useDispatch();
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight = Dimensions.get('window').height;
  const onStarRatingPress = rating => {
    setStarRate(rating);
  };
  const onTitleChange = val => {
    setTitle(val);
  };
  const onReviewChange = val => {
    setReview(val);
  };

  const handleRatesubmit = async () => {
    dispatch({type: PROCCESS});
    try {
      const token = await AsyncStorage.getItem('userToken');
      const res = await Axios.post(
        `https://movie-review-apps-mp-team-g.herokuapp.com/api/v1/dmovies/${id}/review`,
        {
          title: title,
          description: review,
          rating: starRate,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        alert('Rate Success');
        dispatch({type: SUCCESS});
        dispatch({type: COMMENT});
      } else {
        dispatch({type: FAILED});
      }
    } catch (error) {
      console.log(error, 'error');
      dispatch({type: FAILED});
    }
    console.log(title);
    console.log(review);
    console.log(starRate);
    console.log(id);
  };

  return (
    <Modal
      isVisible={modal}
      onBackdropPress={() => dispatch({type: COMMENT})}
      style={styles.modal}
      deviceHeight={deviceHeight}
      deviceWidth={deviceWidth}>
      <View style={styles.modalContainer}>
        <Text style={styles.title}>How do you think about this movie?</Text>
        <StarRating
          disabled={false}
          emptyStar={'ios-star-outline'}
          fullStar={'ios-star'}
          iconSet={'Ionicons'}
          maxStars={10}
          rating={starRate}
          selectedStar={rating => onStarRatingPress(rating)}
          fullStarColor={'rgba(255, 194, 0, 0.98)'}
          containerStyle={styles.ratingContainer}
        />
        <Text style={styles.title}>Your Rating: {starRate}</Text>
        <TextInput
          placeholder="Write a headline for your review here"
          placeholderTextColor="#979797"
          style={styles.inputTitle}
          maxLength={40}
          onChangeText={val => onTitleChange(val)}
        />
        <TextInput
          placeholder="Write your review here"
          placeholderTextColor="#979797"
          style={styles.inputReview}
          maxLength={200}
          onChangeText={val => onReviewChange(val)}
          multiline={true}
        />
        <Button
          title="Submit"
          buttonStyle={styles.button}
          onPress={handleRatesubmit}
        />
        <Button
          title="Cancel"
          buttonStyle={styles.button}
          onPress={() => dispatch({type: COMMENT})}
        />
      </View>
    </Modal>
  );
};

export default ModalRate;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  modal: {
    backgroundColor: '#FFE7AB',
    borderRadius: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    maxHeight: 500,
  },
  title: {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 16,
    letterSpacing: -0.2,
    color: '#000',
    textAlign: 'center',
    marginVertical: 10,
  },
  modalContainer: {
    margin: 20,
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center'
  },
  inputTitle: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    marginVertical: 10,
    textAlign: 'justify',
  },
  inputReview: {
    backgroundColor: '#fff',
    borderRadius: 10,
    padding: 10,
    marginVertical: 10,
    maxHeight: 150,
    height: 101,
    textAlign: 'justify',
    textAlignVertical: 'top',
  },
  button: {
    borderRadius: 25,
    alignSelf: 'center',
    width: 150,
    margin: 10,
  },
  ratingContainer: {
    alignSelf: 'center',
  },
});
