import {
  PROCCESS,
  SUCCESS,
  FAILED,
  RESTORE_TOKEN,
  GET_DATA,
  DETAIL,
  COMMENT,
  GENRE,
  GENRE_NAME,
  COMMENT_EDIT,
  SEARCH,
  ISLOGIN,
  ISLOGOUT,
  ALERT,
} from './case';

const initialState = {
  user: {},
  data: [],
  // name: '',
  // description: '',
  // isLogin: false,
  token: '',
  isLoading: false,
  message: '',
  id: '',
  isComment: false,
  genreID: '',
  genreName: '',
  isCommentEdit: false,
  title: '',
  isLogin: false,
  alert: false,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    // case 'ERROR':
    //     return {...state, description: action.payload};
    // case 'SAVE':
    //     return {...state, data: action.payload};
    // case 'CHANGE_NAME':
    //     return {...state, name: action.payload}
    // case 'LOGOUT':
    //     return {...state, isLogin: action.payload }
    // case LOGIN:
    //     return {...state, token: action.payload}
    // case LOADING:
    //     return {...state, isLoading: action.payload}
    case PROCCESS:
      return {
        ...state,
        isLoading: true,
      };
    case SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case FAILED:
      return {
        ...state,
        isLoading: false,
        message: alert('Invalid Input'),
      };
    case RESTORE_TOKEN:
      return {
        ...state,
        token: action.payload,
        isLoading: false,
      };
    case GET_DATA:
      return {
        ...state,
        data: action.payload,
      };
    case DETAIL:
      return {
        ...state,
        id: action.payload,
      };
    case COMMENT:
      return {
        ...state,
        isComment: !state.isComment,
      };
    case GENRE:
      return {
        ...state,
        genreID: action.payload,
      };
    case GENRE_NAME:
      return {
        ...state,
        genreName: action.payload,
      };
    case COMMENT_EDIT:
      return {
        ...state,
        isCommentEdit: !state.isCommentEdit,
      };
    case SEARCH:
      return {
        ...state,
        title: action.payload,
      };
    case ISLOGIN:
      return {
        ...state,
        isLogin: true,
      };
    case ISLOGOUT:
      return {
        ...state,
        isLogin: false,
      };
    case ALERT:
      return {
        ...state,
        alert: !state.alert,
      };
    default:
      return state;
  }
};
