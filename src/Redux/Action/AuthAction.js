// export const save_data = (data) => {
//     return{
//         type: 'SAVE',
//         payload: data,
//     }
// }

import {FAILED, SUCCESS, PROCESS, GET_DATA} from '../Reducer/case';
import {Share} from 'react-native';

// export const change_name = (params) =>{
//     return{
//         type: 'CHANGE_NAME',
//         payload: params,
//     }
// }

// export const handleLogout = (state) => {
//     return{
//         type: 'LOGOUT',
//         payload: state
//     }
// }

// export const handleLogin = async (token) => {
//     return{
//         type: 'LOGIN',
//         payload: token
//     }
// }

// export const handleLoading = (state) => {
//     return{
//         type: "LOADING",
//         payload: state
//     }
// }

export const handleProcces = state => {
  return {
    type: PROCESS,
  };
};
export const handleSuccess = state => {
  return {
    type: SUCCESS,
  };
};
export const handleFailed = state => {
  return {
    type: FAILED,
  };
};

export const getData = params => {
  return {
    type: GET_DATA,
    payload: params,
  };
};

