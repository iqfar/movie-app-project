/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';
import Profile from './profile screen/Profile';
import HomeRouter from './main screen/Router';
import MyReview from './review screens/MyReview';
import HomeLogin from './login screen';
import {useSelector} from 'react-redux';
const Tab = createBottomTabNavigator();

const MyTab = () => {
  const isLogin = useSelector(state => state.auth.isLogin);
  return (
    <Tab.Navigator initialRouteName="Home">
      <Tab.Screen
        name="Review"
        component={isLogin == true ? MyReview : HomeLogin}
        options={{
          tabBarIcon: ({focused}) => {
            let iconName;
            iconName = focused ? 'comment-text' : 'comment-text-outline';
            return <Icon type="material-community" name={iconName} size={30} />;
          },
        }}
      />
      <Tab.Screen
        name="Home"
        component={HomeRouter}
        options={{
          tabBarIcon: ({focused}) => {
            let iconName;
            iconName = focused ? 'home' : 'home-outline';
            return <Icon type="material-community" name={iconName} size={30} />;
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={isLogin == true ? Profile : HomeLogin}
        options={{
          tabBarIcon: ({focused}) => {
            let iconName;
            iconName = focused ? 'account-circle' : 'account-circle-outline';
            return <Icon type="material-community" name={iconName} size={30} />;
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default MyTab;
