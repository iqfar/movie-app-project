import React, {useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MyTab from './MyTab';
import Profile from './profile screen/Profile';
import EditPassword from './profile screen/EditPassword';
import Review from './review screens/Review';
import HomeLogin from './login screen';
import Login from './login screen/Login';
import Register from './login screen/Register';
import HomeDetail from './main screen/HomeDetail';
import CardItemList from './main screen/CardItemList';
import HomeSearch from './main screen/HomeSearch';
import CardItemDetail from './main screen/CardItemDetail';
import MyReview from './review screens/MyReview';
import EditImage from './profile screen/EditProfile';
import SplashScreen from './SplashScreen';
import Alert from './Alert';
import AppNavigator from './AppNavigator';

const Stack = createStackNavigator();

const MyStackRoot = () => {
  const [isLogin] = useState(false);

  console.log(isLogin);

  return (
    <Stack.Navigator initialRouteName="Splash Screen">
      <Stack.Screen
        name="Splash Screen"
        component={SplashScreen}
        options={{headerShown: false}}
      />
      {/* <Stack.Screen name='Main Navigator' component={AppNavigator} options={{headerShown: false}} /> */}
      <Stack.Screen
        name="Home Login"
        component={HomeLogin}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Alert"
        component={Alert}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="My Tab"
        component={MyTab}
        options={{headerShown: false}}
      />
      {/* <Stack.Screen
        name="Home Details"
        component={HomeDetail}
        options={{headerShown: false}}
      /> */}
      {/* <Stack.Screen
        name="CardItemList"
        component={CardItemList}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home Search"
        component={HomeSearch}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Card Item Detail"
        component={CardItemDetail}
        options={{headerShown: false}}
      /> */}
      {/* <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      /> */}
      <Stack.Screen
        name="All Review"
        component={Review}
        options={{headerShown: true}}
      />
      <Stack.Screen
        name="My Review"
        component={MyReview}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Edit Password"
        component={EditPassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Edit Image"
        component={EditImage}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default MyStackRoot;
